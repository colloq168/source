package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	cidrs := []string{
		"162.159.192.0/24",
		"162.159.193.0/24",
		"162.159.195.0/24",
		"188.114.96.0/24",
		"188.114.97.0/24",
		"188.114.98.0/24",
		"188.114.99.0/24",
	}

	var ips []string
	for _, cidr := range cidrs {
		ipList, err := getIPsFromCIDR(cidr)
		if err != nil {
			fmt.Println(err)
			continue
		}
		ips = append(ips, ipList...)
	}

	if err := saveIPsToFile("ip.txt", ips); err != nil {
		fmt.Println(err)
		return
	}
}

func getIPsFromCIDR(cidr string) ([]string, error) {
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}
	var ips []string
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		ips = append(ips, ip.String())
	}
	return ips, nil
}

func saveIPsToFile(fileName string, ips []string) error {
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()
	for _, ip := range ips {
		if _, err := file.WriteString(ip + "\n"); err != nil {
			return err
		}
	}
	return nil
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}
